﻿SET NAMES 'utf8';

DROP DATABASE IF EXISTS practica7; 
CREATE DATABASE practica7;
USE practica7; 

CREATE TABLE alumno(
  mat int AUTO_INCREMENT,
  nombre varchar(200),
  grupo varchar(20),
  PRIMARY KEY(mat)
  );

CREATE TABLE realiza(
  `mat-alumno` int,
  `p#practica` int,
  PRIMARY KEY(`mat-alumno`,`p#practica`)
  );

CREATE TABLE practica(
  `p#` int AUTO_INCREMENT,
  titulo varchar(100),
  dificultad varchar(100),
  PRIMARY KEY(`p#`)
  );

CREATE TABLE fechaP(
  `mat-alumno`int,
  `p#practica` int,
  nota float,
  fecha date,
  PRIMARY KEY(`mat-alumno`,`p#practica`)
  );


ALTER TABLE realiza
  ADD CONSTRAINT fkRealizaAlumno
    FOREIGN KEY (`mat-alumno`)
    REFERENCES alumno(mat)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkRealizaPractica
    FOREIGN KEY (`p#practica`)
    REFERENCES practica(`p#`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;


ALTER TABLE fechaP
  ADD CONSTRAINT fkFechaPRealiza
    FOREIGN KEY (`mat-alumno`,`p#practica`)
    REFERENCES realiza(`mat-alumno`,`p#practica`)
    ON DELETE RESTRICT ON UPDATE RESTRICT;


