﻿SET NAMES 'utf8';
USE practica6;

-- almacenando el estado de las claves ajenas
-- en la variable estado
SET @estado=@@foreign_key_checks;
-- deshabilito las claves ajenas
SET FOREIGN_KEY_CHECKS=FALSE;

/** 
  ahora puedo meter los datos en el orden que me de la gana
 **/

  INSERT INTO cliente 
    (nif, nombre, direccion, ciudad, tfno) VALUES 
    ('20202020a', 'Jorge','alta', 'santander', '456456456'),
    ('10101010b','Ana','luis','Loredo',NULL);

  INSERT INTO compra 
    (`nif-cliente`, `matricula-coche`) VALUES 
    ('20202020a', '1234aaa'),
    ('10101010b','1111bbb');

  INSERT INTO coche 
    (matricula, marca, modelo, color, precio) VALUES 
    ('1234aaa', 'ford', 'cupra', 'rojo', 10000),
    ('1111bbb','toyota','celica','rojo',10000),
    ('0000ccc','ford','mondeo','blanco',5000);

  INSERT INTO revision 
    (filtro, aceite, frenos, `matricula-coche`) VALUES 
    ('1', '1', '0', '1234aaa');


-- vuelvo a dejar las claves ajenas como estaban inicialmente
SET FOREIGN_KEY_CHECKS=@estado;


SELECT * FROM coche c;
SELECT * FROM cliente c;
SELECT * FROM revision r;

