﻿SET NAMES 'utf8';

USE practica7;

SET @copia=@@foreign_key_checks;
SET FOREIGN_KEY_CHECKS=FALSE;

INSERT INTO alumno 
  (mat, nombre, grupo) VALUES 
  (1, 'susana', 'a'),
  (2,'ana','a');

INSERT INTO realiza 
  (`mat-alumno`, `p#practica`) VALUES 
  (1,1),
  (2,1),
  (1,2),
  (1,3);

INSERT INTO practica 
  (`p#`, titulo, dificultad) VALUES 
  (1, 'sql', 'baja'),
  (2,'lmd','media'),
  (3,'ldd','alta');

INSERT INTO fechap 
  (`mat-alumno`, `p#practica`, nota, fecha) VALUES 
  (1, 1, 8, CURDATE()),
  (1,2,6,CURDATE());
  




SET FOREIGN_KEY_CHECKS=@copia;