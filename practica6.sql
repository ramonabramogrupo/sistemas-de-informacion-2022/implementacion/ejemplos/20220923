﻿DROP DATABASE IF EXISTS practica6; 
CREATE DATABASE practica6;
USE practica6; 

CREATE TABLE cliente(
  nif varchar(10),
  nombre varchar(100),
  direccion varchar(500),
  ciudad varchar(100),
  tfno varchar(20) COMMENT 'telefono del cliente completo'
  );

CREATE TABLE coche(
  matricula varchar(10),
  marca varchar(100),
  modelo varchar(100),
  color varchar(100),
  precio float
  ) COMMENT 'coches vendidos';

CREATE TABLE compra(
  `nif-cliente` varchar(10),
  `matricula-coche` varchar(10)
);

CREATE TABLE revision(
  codigo int,
  filtro varchar(100),
  aceite varchar(100),
  frenos varchar(100),
  `matricula-coche` varchar(10)
  );

ALTER TABLE cliente
  ADD PRIMARY KEY (nif);

ALTER TABLE coche 
  ADD PRIMARY KEY(matricula);

ALTER TABLE compra
  ADD PRIMARY KEY(`nif-cliente`,`matricula-coche`),
  ADD UNIQUE KEY (`matricula-coche`);
  

ALTER TABLE revision
  ADD PRIMARY KEY (codigo),
  CHANGE COLUMN codigo codigo int AUTO_INCREMENT;
  
ALTER TABLE compra 
  ADD CONSTRAINT fkCompraCliente
    FOREIGN KEY (`nif-cliente`)
    REFERENCES cliente(nif)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkCompraCoche
    FOREIGN KEY (`matricula-coche`)
    REFERENCES coche(matricula)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE revision 
  ADD CONSTRAINT fkRevisionCoche
    FOREIGN KEY (`matricula-coche`)
    REFERENCES coche(matricula)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

