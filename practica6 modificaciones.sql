﻿SET NAMES 'utf8';

USE practica6;

ALTER TABLE revision 
  CHANGE COLUMN filtro filtro bool,
  MODIFY COLUMN aceite bool,
  CHANGE COLUMN frenos frenos bool;

-- SELECT * FROM revision r;
-- DESCRIBE revision;

ALTER TABLE coche
  DROP COLUMN precio;

SELECT * FROM coche;

ALTER TABLE cliente 
  CHANGE COLUMN tfno telefono varchar(20);

SELECT * FROM cliente;

-- tengo que borrar la clave ajena
-- porque si no no puedo eliminar el unique key
-- otra opcion seria deshabilitar las claves ajenas
ALTER TABLE compra
  DROP FOREIGN KEY fkCompraCoche;

ALTER TABLE compra
  DROP KEY `matricula-coche`;

-- volver a crear la clave ajena borrada
ALTER TABLE compra
  ADD CONSTRAINT fkCompraCoche
    FOREIGN KEY (`matricula-coche`)
    REFERENCES coche(matricula)
    ON DELETE RESTRICT ON UPDATE RESTRICT;




